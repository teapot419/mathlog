package ru.ifmo.ctddev.maltsev.mathlogic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class ExpressionUtils {
    private static final List<Lexem> lexems = Arrays.
            stream(Lexem.values()).
            filter(x -> x.chars != null).
            collect(Collectors.toList());

    private ExpressionUtils() {

    }

    public static List<String> tokenize(String s) {
        List<String> answer = new ArrayList<>();
        SimpleTokenizer tokenizer = new SimpleTokenizer(s);
        while (tokenizer.hasTokens()) {
            for (Lexem lexem : lexems) {
                if (tokenizer.hasNext(lexem.chars)) {
                    answer.add(tokenizer.next(lexem.chars));
                }
            }
            String tmp = tokenizer.nextAlnum();
            if (!tmp.isEmpty()) {
                answer.add(tmp);
            }
        }
        return answer;
    }

    private static class SimpleTokenizer {
        String data;
        int position;

        public SimpleTokenizer(String data) {
            data = data.replace(" ", "");
            this.data = data;
            this.position = 0;
        }

        public boolean hasNext(String str) {
            if (position + str.length() > data.length()) {
                return false;
            }
            return data.substring(position, position + str.length()).equals(str);
        }

        public String next(String str) {
            if (hasNext(str)) {

                String result = data.substring(position, position + str.length());
                position += str.length();
                return result;
            }
            return null;
        }

        public String nextAlnum() {
            StringBuilder result = new StringBuilder();
            while (position < data.length()) {
                Character c = data.charAt(position);
                if (!Character.isAlphabetic(c) && !Character.isDigit(c)) {
                    break;
                }
                result.append(c);
                position++;
            }
            return result.toString();
        }


        public boolean hasTokens() {
            return position < data.length();
        }
    }
}
