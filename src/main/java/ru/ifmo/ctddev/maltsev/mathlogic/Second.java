package ru.ifmo.ctddev.maltsev.mathlogic;


import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public final class Second {
    private Second() {

    }

    public static void main(String[] args) throws IOException {
        List<String> data = Files.readAllLines(Paths.get("test.in"));
        System.setOut(new PrintStream("test.out"));
        List<String> proof = data.stream().skip(1).collect(Collectors.toList());
        String title = data.get(0).trim();
        String[] proposals = title.substring(0, title.indexOf("|-")).split(",");
        String alpha = proposals[proposals.length - 1];
        String beta = title.substring(title.indexOf("|-") + 2);
        StringJoiner joiner = new StringJoiner(",");
        for (int i = 0; i < proposals.length - 1; ++i) {
            joiner.add(proposals[i]);
        }
        System.out.print(joiner.toString());
        System.out.print("|-");
        System.out.printf("%s->%s%n", alpha, beta);
        List<ParserTreeNode> result = new Deduction(title, proof).deduct();
        for (ParserTreeNode node : result) {
            System.out.println(node.toString());
        }

    }
}
