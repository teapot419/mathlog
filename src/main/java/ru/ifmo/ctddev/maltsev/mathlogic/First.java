package ru.ifmo.ctddev.maltsev.mathlogic;


import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class First {
    private First() {

    }

    public static void main(String... args) throws IOException {
        List<String> data = Files.readAllLines(Paths.get("test.in"));
        System.setOut(new PrintStream("test.out"));
        List<ParserTreeNode> parsedTreeNodes = data.stream().
                map(LogicGrammarParser::new).
                map(LogicGrammarParser::parse).
                collect(Collectors.toList());
        List<ParserTreeNode> hypothesis = new ArrayList<>();
        Map<ParserTreeNode, List<Integer>> cons = new HashMap<>();
        Map<ParserTreeNode, Integer> total = new HashMap<>();
        int j = 0;
        for (ParserTreeNode node : parsedTreeNodes) {
            boolean proved = false;
            System.out.printf("%s %s ", j + 1, node);
            int pos = Axioms.findAxiom(node);
            if (pos != -1) {
                System.out.printf("Сх. акс. %s%n", pos + 1);
                proved = true;
            } else {

                if (hypothesis.contains(node)) {
                    System.out.printf("Предп. %s%n", hypothesis.indexOf(node));
                    proved = true;
                } else {
                    if (cons.containsKey(node)) {
                        for (int h : cons.get(node)) {
                            if (h < j && total.containsKey(parsedTreeNodes.get(h).left)) {
                                System.out.printf("M.P. %s, %s%n", total.get(parsedTreeNodes.get(h).left) + 1, h + 1);
                                proved = true;
                                break;
                            }
                        }
                    }
                }


            }
            if (proved) {
                total.put(node, j);
                if (node.lexem != Lexem.NEGATE && node.lexem != Lexem.VARIABLE) {
                    cons.putIfAbsent(node.right, new ArrayList<>());
                    cons.get(node.right).add(j);
                }
            } else {
                System.out.println("Не доказано");
            }
            j++;
        }
    }
}
