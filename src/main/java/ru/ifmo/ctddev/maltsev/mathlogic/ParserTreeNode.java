package ru.ifmo.ctddev.maltsev.mathlogic;


import java.util.Objects;

public class ParserTreeNode

{
    Lexem lexem;
    ParserTreeNode left;
    ParserTreeNode right;
    String tag;
    private int hashCode;

    public ParserTreeNode(Lexem lexem, ParserTreeNode left, ParserTreeNode right, String tag) {
        this.lexem = lexem;
        this.left = left;
        this.right = right;
        this.tag = tag;
        this.hashCode = Objects.hash(this.lexem, this.tag, this.left, this.right);
    }

    public ParserTreeNode(Lexem lexem, ParserTreeNode left, ParserTreeNode right) {
        this(lexem, left, right, null);
    }

    public static ParserTreeNode createVariableNode(String name) {
        return new ParserTreeNode(Lexem.VARIABLE, null, null).setTag(name);
    }

    public ParserTreeNode setTag(String name) {
        this.tag = name;
        return this;
    }

    @Override
    public String toString() {
        if (lexem == Lexem.VARIABLE) {
            return String.format("%s", tag);
        } else if (lexem == Lexem.NEGATE) {
            if (left.lexem == Lexem.NEGATE || left.lexem == Lexem.VARIABLE) {
                return String.format("%s%s", this.lexem.chars, this.left.toString());
            }
            return String.format("%s(%s)", this.lexem.chars, this.left.toString());
        }

        return String.format("(%s%s%s)", left.toString(), lexem.chars, right.toString());
    }

    @Override
    public int hashCode() {
        return this.hashCode;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }

        ParserTreeNode node = (ParserTreeNode) other;
        return node.lexem == this.lexem && Objects.equals(node.tag, this.tag) &&
                Objects.equals(this.left, node.left) && Objects.equals(this.right, node.right);
    }
}
