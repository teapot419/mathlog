package ru.ifmo.ctddev.maltsev.mathlogic;

import java.util.Objects;

public enum Lexem {
    IMPLICATION("->", 0), DISJUNCTION("|", 1), CONJUNCTION("&", 2), NEGATE("!", 3),
    BRACKET_OPENED("(", -1, "\\("), BRACKET_CLOSED(")", -1, "\\)"), VARIABLE(null);

    public final String chars;
    public final int priority;
    public final String escaped;

    Lexem(String chars, int priority, String escaped) {
        this.chars = chars;
        this.priority = priority;
        this.escaped = escaped;
    }

    Lexem(String chars, int priority) {
        this(chars, priority, chars);
    }

    Lexem(String chars) {
        this(chars, 0);
    }

    public static Lexem getLexem(String chars) {
        for (Lexem lexem : values()) {
            if (Objects.equals(chars, lexem.chars)) {
                return lexem;
            }

        }
        return Lexem.VARIABLE;
    }

}
