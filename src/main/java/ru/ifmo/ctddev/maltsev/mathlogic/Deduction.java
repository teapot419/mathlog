package ru.ifmo.ctddev.maltsev.mathlogic;

import java.util.ArrayList;
import java.util.List;

public class Deduction {
    String title;
    List<String> proof;

    public Deduction(String title, List<String> proof) {
        this.title = title;
        this.proof = proof;
    }

    public List<ParserTreeNode> deduct() {
        ArrayList<ParserTreeNode> proposals = new ArrayList<>();
        ArrayList<ParserTreeNode> proven = new ArrayList<>();
        ArrayList<ParserTreeNode> result = new ArrayList<>();

        int pos = title.indexOf("|-");
        String[] rawProposals = title.substring(0, pos).split(",");
        for (String rawExpression : rawProposals) {
            ParserTreeNode parsedExpression = LogicGrammarParser.parse(rawExpression);
            proposals.add(parsedExpression);
        }
        ParserTreeNode alpha = proposals.remove(proposals.size() - 1);
        for (String rawExpression : proof) {
            ParserTreeNode expression = LogicGrammarParser.parse(rawExpression);

            if (proposals.indexOf(expression) != -1 || Axioms.findAxiom(expression) != -1) {
                result.add(expression);
                result.add(new ParserTreeNode(Lexem.IMPLICATION, expression,
                        new ParserTreeNode(Lexem.IMPLICATION, alpha, expression)));
                result.add(new ParserTreeNode(Lexem.IMPLICATION, alpha, expression));
            } else if (expression.equals(alpha)) {
                ParserTreeNode t1 = new ParserTreeNode(Lexem.IMPLICATION, alpha,
                        new ParserTreeNode(Lexem.IMPLICATION,
                                new ParserTreeNode(Lexem.IMPLICATION, alpha, alpha),
                                alpha));
                ParserTreeNode t2 = new ParserTreeNode(Lexem.IMPLICATION, alpha,
                        new ParserTreeNode(Lexem.IMPLICATION, alpha, alpha));
                result.add(t1);
                result.add(t2);
                result.add(new ParserTreeNode(Lexem.IMPLICATION, t2,
                        new ParserTreeNode(Lexem.IMPLICATION, t1,
                                new ParserTreeNode(Lexem.IMPLICATION, alpha, alpha))));
                result.add(new ParserTreeNode(Lexem.IMPLICATION, t1,
                        new ParserTreeNode(Lexem.IMPLICATION, alpha, alpha)));
                result.add(new ParserTreeNode(Lexem.IMPLICATION, alpha, alpha));

            } else {
                ParserTreeNode mp1 = ParserTreeNode.createVariableNode("FUCK");
                LOOP:
                for (int i = proven.size() - 1; i >= 0; --i) {
                    ParserTreeNode node = proven.get(i);
                    if (node.lexem == Lexem.IMPLICATION && node.right.equals(expression)) {
                        for (int j = proven.size() - 1; j >= 0; --j) {
                            ParserTreeNode node2 = proven.get(j);
                            if (node2.equals(node.left)) {
                                mp1 = node2;
                                break LOOP;

                            }
                        }
                    }

                }

                ParserTreeNode t1 = new ParserTreeNode(Lexem.IMPLICATION, alpha, mp1);
                ParserTreeNode t2 = new ParserTreeNode(Lexem.IMPLICATION, alpha,
                        new ParserTreeNode(Lexem.IMPLICATION, mp1, expression));
                ParserTreeNode t3 = new ParserTreeNode(Lexem.IMPLICATION, alpha, expression);
                result.add(new ParserTreeNode(Lexem.IMPLICATION, t1,
                        new ParserTreeNode(Lexem.IMPLICATION, t2, t3)));
                result.add(new ParserTreeNode(Lexem.IMPLICATION, t2, t3));
                result.add(t3);
            }
            proven.add(expression);

        }
        return result;

    }
}
