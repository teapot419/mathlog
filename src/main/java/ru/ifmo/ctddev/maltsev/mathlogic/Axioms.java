package ru.ifmo.ctddev.maltsev.mathlogic;

import java.util.*;
import java.util.stream.Collectors;

public class Axioms {
    private static final String[] FORMULAE = {"A->B->A", "(A->B)->(A->B->C)->(A->C)", "A->B->A&B",
            "A&B->A", "A&B->B", "A->A|B", "B->A|B",
            "(A->C)->(B->C)->(A|B->X)", "(A->B)->(A->!B)->!A", "!!A->A"};
    public static final List<ParserTreeNode> PARSED_AXIOMS = Arrays.stream(FORMULAE).
            map(LogicGrammarParser::new).
            map(LogicGrammarParser::parse).
            collect(Collectors.toList());

    private Axioms() {

    }

    public static int findAxiom(ParserTreeNode node) {
        for (int i = 0; i < PARSED_AXIOMS.size(); i++) {
            if (compareToAxiom(PARSED_AXIOMS.get(i), node, new HashMap<>())) {
                return i;
            }
        }
        return -1;
    }


    private static boolean compareToAxiom(ParserTreeNode axiomNode, ParserTreeNode node, Map<String, ParserTreeNode> variables) {
        if (node.lexem == Lexem.VARIABLE || node.lexem != axiomNode.lexem) {
            return false;
        }
        if (axiomNode.left.lexem == Lexem.VARIABLE) {
            ParserTreeNode tmpNode = variables.putIfAbsent(axiomNode.left.tag, node.left);
            if (tmpNode != null && !Objects.equals(tmpNode, node.left)) {
                return false;
            }
        } else {
            if (!compareToAxiom(axiomNode.left, node.left, variables)) {
                return false;
            }
        }

        if (axiomNode.lexem != Lexem.NEGATE) {
            if (axiomNode.right.lexem == Lexem.VARIABLE) {
                ParserTreeNode tmpNode = variables.putIfAbsent(axiomNode.right.tag, node.right);
                if (tmpNode != null) {
                    return Objects.equals(tmpNode, node.right);
                }
            } else {
                return compareToAxiom(axiomNode.right, node.right, variables);
            }
        }
        return true;
    }
}
