package ru.ifmo.ctddev.maltsev.mathlogic;


import java.util.List;
import java.util.Stack;

public class LogicGrammarParser {
    private final String data;

    private int n;
    private List<String> tokenized;
    private int pos;

    public LogicGrammarParser(String s) {
        this.data = s;
        this.pos = 0;

    }

    public static ParserTreeNode parse(String data) {
        return new LogicGrammarParser(data).parse();
    }

    private String current() {
        return tokenized.get(pos);
    }

    private Lexem currentLexem() {
        return Lexem.getLexem(current());
    }

    private ParserTreeNode parseInternal() {
        Stack<ParserTreeNode> stack = new Stack<>();
        Stack<Lexem> operations = new Stack<>();
        WHILE:
        while (pos < n) {
            switch (currentLexem()) {
                case NEGATE:
                    int negCount = 0;
                    while (pos < n && currentLexem() == Lexem.NEGATE) {
                        ++pos;
                        ++negCount;
                    }
                    ParserTreeNode node;
                    if (currentLexem() == Lexem.BRACKET_OPENED) {
                        ++pos;
                        node = parseInternal();
                    } else {
                        node = ParserTreeNode.createVariableNode(current());
                    }

                    for (int i = 0; i < negCount; ++i) {
                        node = new ParserTreeNode(Lexem.NEGATE, node, null);
                    }
                    stack.push(node);
                    break;
                case BRACKET_CLOSED:
                    break WHILE;
                case BRACKET_OPENED:
                    pos++;
                    stack.push(parseInternal());
                    break;
                case VARIABLE:
                    stack.add(ParserTreeNode.createVariableNode(current()));
                    break;
                default:
                    ParserTreeNode last = stack.pop();
                    while (!operations.isEmpty()) {
                        Lexem lastOperation = operations.peek();
                        if (lastOperation.priority == 0 || lastOperation.priority < currentLexem().priority) {
                            break;
                        }
                        last = new ParserTreeNode(operations.pop(), stack.pop(), last);
                    }
                    stack.push(last);
                    operations.push(currentLexem());
                    break;
            }
            ++pos;

        }
        ParserTreeNode result = stack.pop();
        while (!operations.isEmpty()) {
            result = new ParserTreeNode(operations.pop(), stack.pop(), result);
        }
        return result;
    }

    public ParserTreeNode parse() {
        this.tokenized = ExpressionUtils.tokenize(data);
        this.n = tokenized.size();
        return parseInternal();
    }
}
